package com.sterenl.exceptionhandler;

import android.content.Context;
import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

    private final static String ERROR_FILE = ExceptionHandler.class.getSimpleName() + ".error";
    private final Context mContext;
    private final Thread.UncaughtExceptionHandler rootHandler;
    private static final String ENCODING = "UTF-8";
    private String mUserId;
    private static ExceptionHandler mInstance;


    // Prevent direct instantiation.
    private ExceptionHandler(Context context, String userId) {
        mContext = context;
        rootHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
        mUserId = userId;
    }


    public static void init(Context context, String userId) {
        if (mInstance == null) {
            synchronized (ExceptionHandler.class) {
                if (mInstance == null) {
                    mInstance = new ExceptionHandler(context, userId);
                }
            }
        }
    }


    @Override
    public void uncaughtException(final Thread thread, final Throwable exception) {
        try {
            File f = new File(mContext.getFilesDir(), ERROR_FILE);
            String dataToUpload = exception.getStackTrace().toString() + " " + exception.getMessage() + " " + exception.getClass().getSimpleName() + " " + System.currentTimeMillis() + " " + mUserId;
            FileUtils.write(f, dataToUpload, ENCODING);
        } catch (Exception e) {
            Log.e(TAG, "Exception Logger failed!", e);
        }

        rootHandler.uncaughtException(thread, exception);
    }

    private static List<String> readExceptions(Context context) {
        List<String> exceptions = new ArrayList<>();
        File errorFile = new File(context.getFilesDir(), ERROR_FILE);
        if (errorFile.exists()) {
            try {
                exceptions = FileUtils.readLines(errorFile, ENCODING);
            } catch (IOException exception) {
                Log.e(TAG, "readExceptions failed!", exception);
            }
        }
        return exceptions;
    }

    public static List<String> getExceptions(Context context) {
        return readExceptions(context);
    }
}


